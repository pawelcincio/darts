class Iterator {

	constructor(limit) {
		this.limit = limit;
		this.value = 0;
	}
	
	get value() {
		return this._value;
	}
	
	set value(value) {
		this._value = value;
	}
	
	move() {
		if (this.value == this.limit - 1) {
			this.value = 0;
		}
		else {
			this.value++;
		}
	}
	
}