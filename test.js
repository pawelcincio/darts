var FINAL_MESSAGE;

function testOnLoad() {
	
	initVariables();
	
	FINAL_MESSAGE = "";
	let testBody = document.getElementById("test-body");
	addMessage("");
	addMessage("START OF TESTS");
	addMessage("");
	
	// do tests
	shouldCalculateEndingInOneShotWhenDoubleEnding();
	shouldCalculateEndingInOneShotWhenNoDoubleEnding();
	shouldCalculateEndingByTripleFiftyWhenDoubleEnding();
	shouldCalculateEndingByMaximumWhenDoubleEnding();
	shouldSumPointsInDoubleEndingCaseSuccessCase();
	shouldSumPointsInDoubleEndingCase();
	// printAllEndings();
	// end of tests
	
	addMessage("");
	addMessage("END OF TESTS");
	
	testBody.innerHTML = FINAL_MESSAGE;
};

function addMessage(msg) {
	FINAL_MESSAGE = FINAL_MESSAGE.concat("&nbsp;", msg, "<br />");
}

function report(condition, testName) {
	addMessage(testName + (condition ? " - SUCCESS" : " - FAILURE"));
}

function reportCondition(actual, expected, testName) {
	addMessage(testName + (actual == expected ? " - SUCCESS" : " - FAILURE ( " + actual + " != " + expected + " )"));
}

function shouldCalculateEndingInOneShotWhenDoubleEnding() {
	
	let result = CalcUtils.calculateEndings(2, true, 3);
	
	report(result[0][0].points == 1 && result[0][0].multi == 2 , arguments.callee.name);
}

function shouldCalculateEndingInOneShotWhenNoDoubleEnding() {
	
	let result = CalcUtils.calculateEndings(1, false, 3);
	
	report(result[0][0].points == 1 && result[0][0].multi == 1 , arguments.callee.name);
}

function shouldCalculateEndingByTripleFiftyWhenDoubleEnding() {
	
	let result = CalcUtils.calculateEndings(150, true, 3);
	let answer = false;
	
	for (let i in result) {
		if (result[i].length == 3
			&& result[i][0].points == 50 && result[i][0].multi == 1
			&& result[i][1].points == 50 && result[i][1].multi == 1
			&& result[i][2].points == 50 && result[i][2].multi == 1) {
			
			answer = true;
			break;
		}
	}
	
	report(answer , arguments.callee.name);
}

function shouldCalculateEndingByMaximumWhenDoubleEnding() {
	
	let result = CalcUtils.calculateEndings(170, true, 3);
	let answer = false;
	
	for (let i in result) {
		if (result[i].length == 3
			&& result[i][0].points == 20 && result[i][0].multi == 3
			&& result[i][1].points == 20 && result[i][1].multi == 3
			&& result[i][2].points == 50 && result[i][2].multi == 1) {
			
			answer = true;
			break;
		}
	}
	
	report(answer , arguments.callee.name);
}

function shouldSumPointsInDoubleEndingCaseSuccessCase() {
	
	let gameStats = {};
	let gameTableHeader = {style: {}};
	let gameTableContent = {style: {}};
	
	let game = new Game(301, ['P1'], gameStats, gameTableHeader, gameTableContent);
	
	game.addShot(new Shot(20, 3));
	game.addShot(new Shot(20, 3));
	game.addShot(new Shot(20, 3));
	game.addTurn();
	game.addShot(new Shot(20, 3));
	game.addShot(new Shot(19, 3));
	game.addShot(new Shot(2, 2));
	game.addTurn();
	
	let points = game.sumPoints();
	
	reportCondition(points, 0, arguments.callee.name);
}

function shouldSumPointsInDoubleEndingCase() {
	
	let gameStats = {};
	let gameTableHeader = {style: {}};
	let gameTableContent = {style: {}};
	
	let game = new Game(301, ['P1'], gameStats, gameTableHeader, gameTableContent);
	
	game.addShot(new Shot(20, 3));
	game.addShot(new Shot(20, 3));
	game.addShot(new Shot(20, 3));
	game.addTurn();
	game.addShot(new Shot(20, 3));
	game.addShot(new Shot(19, 3));
	game.addShot(new Shot(3, 1));
	game.addTurn();
	
	let points = game.sumPoints();
	
	reportCondition(points, 121, arguments.callee.name);
}

function printAllEndings() {
	
	let i, result;
	
	for (i=1 ; i<180 ; i++) {
		result = CalcUtils.calculateEndings(i, true, 3);
		addMessage(i + "|" + result.length);
	}
}