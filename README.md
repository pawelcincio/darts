# PC Darts - application which supports darts game

---

## Changelog (tags)

Changes and tags related with them:

1. v1.0 - Game is available.
2. v2.0 - Editing, suggesting solutions, i18n and parameters.

## TODO

Bugfixes:

1. Clicking 20 sometimes marks 3. - DONE
2. Fix game ending behaviour. - DONE
3. CSS of game table. - DONE
4. Other games than 301 are not working. - DONE
5. Problems with table when webbrowser changes window size. - DONE
6. Refresh all possible endings on board after editing. - DONE
7. Wrong numbers are shown, when finishing turn was failed. - DONE
8. Support false finishing after editing. - DONE
9. Disable clicking on 301, 501, 701 field. - DONE

Refactor:

1. Add comments. - IN PROGRESS
2. Distinguish all reponsibilities between Game and Board. - DONE
3. Introduce good js practices. - DONE
	1. Replace "var" by "let", where possible. - DONE
	2. Template literals and multi-line strings. - DONE

Functionalities:

1. Editing turns. - DONE
2. Calculating possible endings. - DONE
3. Final statistics. - REJECTED
4. Start next game. - REJECTED
5. Calculating points which left, while doing turn. - DONE
6. Parameters: - DONE
	1. Type of game. - DONE
	2. List of players. - DONE
	3. Immediate start of game. - DONE
	4. Language. - DONE
7. Last turn is shown. - DONE
8. Do i18n. - DONE
9. Help page. - DONE
10. Enable game with no double ending.