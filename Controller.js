// main app controller
class Controller {
	
	constructor(typeOfGame, listOfPlayers, startNow, language) {
		
		this.inTheGame = false;
		this.isModalShown = false;
		this.modalWindowController = null;
		this.game = null;
		this.lang = language;
		
		this.helpButton = document.getElementById("help-button");
		this.langSelect = document.getElementById("lang-select");
		this.startForm = document.getElementById("start-form");
		this.canvas = document.getElementById("my-canvas");
		this.gameData = document.getElementById("game-data");
		this.gameStats = document.getElementById("game-stats");
		this.gameTableHeader = document.getElementById("game-table-header");
		this.gameTableContent = document.getElementById("game-table-content");
		this.countButton = document.getElementById("count-button");
		this.playersTable = document.getElementById("start-form-players-table");
		this.startPointsSelect = document.getElementById("start-form-points");
		this.modalWindow = document.getElementById("modal-window");
		this.modalCloseButton = document.getElementById("modal-window-close-button");
		this.modalWindowContent = document.getElementById("modal-window-content");
		this.modalWindowCanvas = document.getElementById("modal-window-canvas");
		this.modalWindowCountButton = document.getElementById("modal-window-count-button");
		
		this.board = null;
		this.players = new Players(this.playersTable, this.lang);
		
		this.readUrlParams(typeOfGame, listOfPlayers, startNow);
		this.loadI18n();
	}
	
	// parse and validate url params (without lang, which is needed in main.js)
	readUrlParams(typeOfGame, listOfPlayers, startNow) {
		
		if (this.validateTypeOfGame(typeOfGame)) {
			this.startPointsSelect.value = parseInt(typeOfGame);
		}
		
		let parsedPlayers = this.validateListOfPlayers(listOfPlayers);
		if (listOfPlayers) {
			this.removePlayer(0);
			this.removePlayer(0);
			for (name of parsedPlayers) {
				this.players.add(name);
			}
		}
		
		if (this.validateStartNow(startNow) && startNow == "true") {
			this.startGame();
		}
	}
	
	validateTypeOfGame(typeOfGame) {
		if (typeOfGame) {
			for (let type of TYPES_OF_GAME) {
				if (type == parseInt(typeOfGame)) {
					return true;
				}
			}
			alert(`Parameter "${PARAMS.TYPE_OF_GAME}" has to have one of theese values: ${TYPES_OF_GAME}.`);
		}
		return false;
	}
	
	validateListOfPlayers(listOfPlayers) {
		if (listOfPlayers) {
			return listOfPlayers.split(this.players.delimiter);
		}
		return false;
	}
	
	validateStartNow(startNow) {
		if (startNow) {
			return startNow;
		}
		return false;
	}
	
	// write game configuratin to window url
	writeUrlParams(startPoints) {
		
		let urlParams = new URLSearchParams(window.location.search);
		
		urlParams.set("typeOfGame", startPoints);
		urlParams.set("players", this.players.toUrlList());
		urlParams.set("startNow", true);
		urlParams.set("lang", this.lang);
		
		window.history.replaceState(null, null, "?" + urlParams.toString());
	}
	
	// load translated text values
	loadI18n() {
		document.getElementById("start-form-title").innerHTML = i18n[this.lang].GAME_PARAMETERS;
		document.getElementById("div-table-left-col").innerHTML = i18n[this.lang].NUMBER_OF_POINTS + ":";
		document.getElementById("start-form-players").innerHTML = i18n[this.lang].LIST_OF_PLAYERS + ":";
		document.getElementById("start-form-add-player-button").innerHTML = i18n[this.lang].ADD_PLAYER;
		document.getElementById("start-form-start-game-button").innerHTML = i18n[this.lang].START_GAME;
		document.getElementById("count-button").innerHTML = i18n[this.lang].COUNT;
		document.getElementById("modal-window-content-title").innerHTML = i18n[this.lang].EDIT_TOUR;
		document.getElementById("modal-window-count-button").innerHTML = i18n[this.lang].SAVE;
	}
	
	// if we are in the game, if true, board should be displayed
	get inTheGame() {
		return this._inTheGame;
	}
	
	set inTheGame(value) {
		this._inTheGame = value;
	}
	
	// action after and of game
	gameIsFinishedCallback(result) {
		this.inTheGame = !result;
		this.countButton.disabled = result;
	}
	
	// radius of board
	getBoardr() {
		return this.board.r;
	}
	
	// radius of board view (square whith board and points)
	getBoardR() {
		return this.board.R;
	}
	
	// redraw view using different window size, called from main.js
	onPageResize() {
		if (this.inTheGame) {
			this.board.recalculate();
			this.board.drawShots(this.game.currentShots, this.game.getEndingsOfCurrentPlayer(3));
			this.game.updateView();
			if (this.isModalShown) {
				this.modalWindowController.onPageResize();
			}
		}
	}
	
	// action of each click of the board
	addShot(shot) {
		if (!this.isModalShown) {
			this.game.addShot(shot);
			if (this.game.currentShotsCount() < 3) {
				this.board.drawShots(this.game.currentShots,
					this.game.getEndingsOfCurrentPlayer(3 - this.game.currentShotsCount()));
			}
			else {
				this.board.drawShots(this.game.currentShots);
			}
		}
	}
	
	// adds new empty player and displays it in view
	addPlayer() {
		this.players.add();
	}
	
	// removes seleced player and updates view
	removePlayer(i) {
		this.players.remove(i);
	}
	
	// start game - view is changed from configuration panel to board and table
	startGame() {
		
		let startPoints = parseInt(
			this.startPointsSelect.options[this.startPointsSelect.selectedIndex].text
		);
		
		if (this.players.validate()) {
		
			// if game is valid, write configuration to url in order to reuse it later
			this.writeUrlParams(startPoints);
			
			this.hideStartForm();
			
			this.board = new Board(this.canvas, this.countButton, new WindowSizeSource());
			this.game = new Game(startPoints, this.players.names, this.gameStats, this.gameTableHeader,
								this.gameTableContent, this.gameIsFinishedCallback.bind(this));
			
			this.showBoard();
			this.showGameData();
			this.board.draw();
			this.showCountButton();
			this.hideHelpButton();
			this.hideLangSelect();
			
			this.inTheGame = true;
		}
	}
	
	hideStartForm() {
		this.startForm.style.display = "none";
	}
	
	showBoard() {
		this.canvas.style.display = "block";
	}
	
	showGameData() {
		this.gameData.style.display = "block";
	}
	
	showCountButton() {
		this.countButton.style.display = "block";
	}
	
	hideHelpButton() {
		this.helpButton.style.display = "none";
	}
	
	hideLangSelect() {
		this.langSelect.style.display = "none";
	}
	
	// request to count points, no arguments needed, game object aggregates all data
	countButtonClick() {
		this.game.addTurn();
		this.board.draw();
		this.board.drawMarked(this.game.getEndingsOfCurrentPlayer(3));
	}
	
	// displays modal window to edit i-player's j-tour
	pointsCellClick(i, j) {
		this.modalWindow.style.display = "block";
		this.isModalShown = true;
		this.modalWindowController = new ModalWindowController(
				this.modalWindow, this.modalWindowCanvas, this.modalWindowCountButton,
				new ModalWindowSizeSource(this.modalWindowContent), this.game.getShots(i, j).shots, i, j);
	}
	
	closeModalWindow() {
		this.modalWindow.style.display = "none";
		this.isModalShown = false;
	}
	
	// called when board is clicked
	boardClick(event) {
		if (this.inTheGame) {
			this.board.onClick(event.clientX, event.clientY, this);
		}
	}
	
	// called when board in modal window is clicked to edit tour
	modalBoardClick(event) {
		
		let x = this.modalWindowCanvas.offsetLeft;
		let y = this.modalWindowCanvas.offsetTop;
		
		if (this.isModalShown) {
			this.modalWindowController.boardClick(event.clientX - x, event.clientY - y);
		}
	}
	
	// request to change result of tour after editing
	modalSaveButtonClick() {
		
		this.game.editTurn(
			this.modalWindowController.getI(),
			this.modalWindowController.getJ(),
			this.modalWindowController.getCurrentShots());
			
		this.game.updateView();
		this.closeModalWindow();
		
		this.board.drawShots(
			this.game.currentShots, this.game.getEndingsOfCurrentPlayer(3 - this.game.currentShotsCount()));
	}

	// called when we have to add new points, called from board the object,
	// after calculating points from coursor co-ordinates
	onNewPoints(points, multi) {
		this.addShot(new Shot(points, multi));
	}
	
	// displays help page in new window
	helpButtonClick() {
		
		let url = window.location.href;
		let result;
		
		if (url.startsWith("http")) {
			result = window.location.href.replace(window.location.search, "")
				+ "/help/" + this.lang + ".html";
		}
		else {
			result = window.location.href.replace("/index.html" + window.location.search, "")
				+ "/help/" + this.lang + ".html";
		}
		
		window.open(result, "_blank");
	}
	
}