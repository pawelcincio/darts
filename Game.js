class Game {
	
	constructor(startPoints, players, gameStats, gameTableHeader, gameTableContent, gameIsFinishedCallback) {
		this.startPoints = startPoints;
		this.players = players;
		this.gameStats = gameStats;
		this.gameTableHeader = gameTableHeader;
		this.gameTableContent = gameTableContent;
		this.gameIsFinishedCallback = gameIsFinishedCallback;
		this.currentShots = new CurrentShots();
		this.turns = [];
		this.gameIsFinished = false;
		this.doubleEnding = true;
		
		for (let i in this.players) {
			this.turns.push([]);
		}
		
		this.iterator = new Iterator(this.players.length);
		
		this.readCssValues();
		this.updateView();
	}
	
	readCssValues() {
		
		this.cssGameTableCellPadding =
			parseInt(getComputedStyle(document.documentElement).getPropertyValue('--game-table-cell-padding'));
			
		this.cssGameTableCellMargin =
			parseInt(getComputedStyle(document.documentElement).getPropertyValue('--game-table-cell-margin'));
			
		this.cssGameTablePointsCellWidth =
			parseInt(getComputedStyle(document.documentElement).getPropertyValue('--game-table-points-cell-width'));
			
		this.cssGameTableCurrentCellWidth =
			parseInt(getComputedStyle(document.documentElement).getPropertyValue('--game-table-current-cell-width'));
	}
	
	get currentShots() {
		return this._currentShots;
	}
	
	set currentShots(value) {
		this._currentShots = value;
	}
	
	currentShotsCount() {
		return this._currentShots.shots.length;
	}
	
	// recalculates points and builds html table
	updateView() {
		
		let width = this.calculateGameTableWidth() + "px";
		
		this.updateHeader(width);
		this.updateResults();
		this.updateStats();
	}
	
	updateStats() {
		
		let html = "";
		
		if (!this.gameIsFinished) {
			for (let i in this.currentShots.shots) {
				html += "<div id=\"game-table-points-cell\">" + this.currentShots.shots[i].toString() + "</div>";
			}
			let currentTurnResult = Shot.countPoints(this.currentShots.shots);
			html += "<div id=\"game-table-points-cell-bold\">"
				+ (this.sumPoints() - currentTurnResult) + " (" + currentTurnResult + ")</div>";
		}
		
		this.gameStats.innerHTML = html;
	}
	
	sumPoints() {
		
		let tmpShots;
		let tmp;
		
		let i = this.iterator.value;
		
		let points = this.startPoints;
		
		for (let j in this.turns[i]) {
			
			tmpShots = this.turns[i][j].shots;
			tmp = Shot.countPoints(tmpShots);
			
			if (
				points > 0 && (
					(!this.doubleEnding && points >= tmp)
					|| (this.doubleEnding && (points - tmp > 1
						|| (points - tmp == 0 && tmpShots[tmpShots.length - 1].multi == 2)))
					)
				) {
					
				points -= tmp;
			}
		}
		
		return points;
	}
	
	updateHeader(width) {
		
		let html = "";
		for (let i in this.players) {
			html += "<div id=\"game-table-column\">";
			html += "<div id=\"game-table-cell-bold\">";
			html += this.players[i];
			html += "</div>";
			html += "</div>";
		}
		
		this.gameTableHeader.innerHTML = html;
		this.gameTableHeader.style.width = width;
	}
		
	updateResults(width) {
		let results = this.calculateResults();
		this.displayResults(results);
	}
	
	calculateResults() {
		
		let values = [];
		let winners = [];
		let pointsLeft;
		let points;
		let tmpShots;
		let i;
		let j;
		
		for (i in this.players) {
			
			values.push([]);
			winners.push(false);
			pointsLeft = this.startPoints;
			
			for (j in this.turns[i]) {
				
				points = Shot.countPoints(this.turns[i][j].shots);
				
				
				if (points == pointsLeft) {
					// equal
					tmpShots = this.turns[i][j].shots;
					if (tmpShots[tmpShots.length - 1].multi == 2) {
						values[i].push(pointsLeft + " (" + points + ")");
						winners[i] = true;
						//continue; no need
					}
					else {
						values[i].push(pointsLeft + " (" + points + ")");
					}
				}
				else if (points > pointsLeft - 2) {
					// too much
					values[i].push(pointsLeft + " (" + points + ")");
				}
				else {
					// some points left
					pointsLeft -= points;
					values[i].push(pointsLeft + " (" + points + ")");
				}
			}
		}
		
		// move iterator
		let safety = 0;
		while (safety < this.players.length) {
			if (winners[this.iterator.value]) {
				// this player already finished
				this.iterator.move();
				safety++;
			}
			else {
				// this one is ok
				break;
			}
		}
		
		// all players have finished
		this.changeGameFinishStatus(safety == this.players.length);
		
		return {"values": values, "winners": winners};
	}
	
	changeGameFinishStatus(result) {
		this.gameIsFinished = result;
		if (this.gameIsFinishedCallback != undefined) {
			this.gameIsFinishedCallback(result);
		}
	}
	
	displayResults(results) {
		
		let i;
		let j;
		let html = "";
		
		for (i in this.players) {
			
			html += "<div id=\"game-table-column\">";
			
			html += this.buildFirstCell();
			
			for (j in results.values[i]) {
				
				if (j == results.values[i].length - 1) {
					// last value
					if (results.winners[i]) {
						// last value if finishing one
						html += this.buildWinCell(i, j, results.values[i][j]);
					}
					else {
						html += this.buildPointCell(i, j, results.values[i][j]);
					}
				}
				else {
					html += this.buildPointCell(i, j, results.values[i][j]);
				}
			}
			
			// yellow cell
			if (!this.gameIsFinished && this.iterator.value == i) {
				html += this.buildYellowCell();
			}
			
			html += "</div>";
		}
		
		this.gameTableContent.style.width = this.calculateGameTableWidth() + "px";
		this.gameTableContent.style.height = this.calculateGameTableContentHeight() + "px";
		this.gameTableContent.innerHTML = html;
		this.gameTableContent.scrollTop = this.gameTableContent.scrollHeight;
	}

	buildFirstCell() {
		return "<div id=\"game-table-first-cell\">" + this.startPoints + "</div>";
	}
	
	buildPointCell(i, j, value) {
		return "<div id=\"game-table-cell\" onclick=\"controller.pointsCellClick(" + i + ", " + j + ")\">"
			+ value + "</div>";
	}
	
	buildWinCell(i, j, value) {
		return "<div id=\"game-table-win-cell\" onclick=\"controller.pointsCellClick(" + i + ", " + j + ")\">"
			+ value + "</div>";
	}
	
	buildYellowCell() {
		return "<div id=\"game-table-yellow-cell\">&nbsp;</div>";
	}
	
	calculateGameTableWidth() {
		return this.players.length * (this.cssGameTableCurrentCellWidth + 2 * (this.cssGameTableCellPadding
			+ this.cssGameTableCellMargin)) + 18;
	}
	
	calculateGameTableContentHeight() {
		let body = document.body,
			html = document.documentElement;

		return Math.max(
			body.scrollHeight,
			body.offsetHeight,
			html.clientHeight,
			html.scrollHeight,
			html.offsetHeight
		) - 90;
	}
	
	addTurn() {
		
		// do nothing if the game is finished
		if (this.gameIsFinished) {
			return;
		}
		
		this.turns[this.iterator.value].push(this.currentShots.getTurn());
		this.iterator.move();
		this.currentShots.clear();
		this.updateView();
	}
	
	getEndingsOfCurrentPlayer(numberOfTurns) {
		let sum = this.sumPoints() - Shot.countPoints(this.currentShots.shots);
		if (sum <= 180) {
			return CalcUtils.calculateEndings(sum, this.doubleEnding, numberOfTurns);
		}
		else {
			return [];
		}
	}
	
	addShot(shot) {
		this.currentShots.add(shot);
		this.updateStats();
	}
	
	getShots(i, j) {
		return this.turns[i][j];
	}
	
	editTurn(i, j, currentShots) {
		this.turns[i][j] = currentShots;
	}
	
	isDoubleEnding() {
		return this.doubleEnding;
	}
	
}