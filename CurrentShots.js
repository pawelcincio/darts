class CurrentShots {
	
	constructor() {
		this.clear();
	}
	
	get shots() {
		return this._shots;
	}
	
	set shots(value) {
		this._shots = value;
	}
	
	clear() {
		this.shots = [];
	}
	
	add(shot) {
		if (this.countOccurrences(shot) >= 3) {
			this.shots = this.filterShots(shot);
		}
		else {
			this.shots.push(shot);
		}
	}
	
	getTurn() {
		return new Turn(this.shots);
	}
	
	countOccurrences(shot) {
		return this.shots.length - this.filterShots(shot).length;
	}
	
	filterShots(shot) {
		
		let result = this.shots.filter(function(obj) {
			return !obj.equals(shot);
		});
		
		if (Array.isArray(result)) {
			return result;
		}
		else {
			return [];
		}
	}
	
}