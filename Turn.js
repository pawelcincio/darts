class Turn {
	
	constructor(shots) {
		this._shots = shots;
	}
	
	set shots(shots) {
		
		if (!Array.isArray(shots)) {
			console.log("Variable \"shots\" has to be an array!");
		}
		else if (shots.length > 3) {
			console.log("More than 3 shots in one turn are not acceptable!");
		}
		else {
			this._shots = shots;
		}
	}
	
	get shots() {
		return this._shots;
	}
	
}