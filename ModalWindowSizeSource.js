class ModalWindowSizeSource {
	
	constructor(modalWindowContent) {
		this.modalWindowContent = modalWindowContent;
	}
	
	getWidth() {
		return Math.round(this.modalWindowContent.clientWidth * 0.84);
	}
	
	getHeight() {
		return Math.round(this.modalWindowContent.clientHeight * 0.84);
	}
	
}