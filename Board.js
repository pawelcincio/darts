class Board {

	constructor(c, countButton, sizeSource) {
		this.canvas = c;
		this.context = this.canvas.getContext("2d");
		this.countButton = countButton;
		this.sizeSource = sizeSource;
		this.recalculate();
	}
	
	recalculate() {
		
		let min = Math.min(this.sizeSource.getWidth(), this.sizeSource.getHeight());
		
		this.canvas.width = min;
		this.canvas.height = min;
		this.canvas.style.width = min + "px";
		this.canvas.style.height = min + "px";
		this.R = min / 2;
		this.boardMargin = CONST.BOARD_MARGIN_RATIO * min;
		this.r = this.R - this.boardMargin;
		this.doubleRingR = (CONST.DOUBLE_RING + CONST.BOARD_END) * this.r / 2;
		this.tripleRingR = (CONST.TRIPLE_RING + CONST.SANDARD_2) * this.r / 2;
		this.standardSize = (CONST.DOUBLE_RING - CONST.SANDARD_2) * this.r;
		this.standardR = CONST.SANDARD_2 * this.r + this.standardSize / 2;
		this.bullR = (CONST.BULL + CONST.SANDARD_1) * this.r / 2;
		this.bullsEyeR = CONST.BULL * this.r;
		this.shotR = CONST.SHOT_R_RATIO * this.r;
	}
	
	get R() {
		return this._R;
	}
	
	set R(value) {
		this._R = value;
	}
	
	get r() {
		return this._r;
	}
	
	set r(value) {
		this._r = value;
	}
	
	draw() {
	
		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	
		let angle = this.getStartAngle();
		
		for (let i in POINTS) {
			this.drawArc(angle, i, CONST.DOUBLE_RING, CONST.BOARD_END, COLOR.TYPE_MULTI, false);// double
			this.drawArc(angle, i, CONST.SANDARD_2, CONST.DOUBLE_RING, COLOR.TYPE_STANDARD, false);
			this.drawArc(angle, i, CONST.TRIPLE_RING, CONST.SANDARD_2, COLOR.TYPE_MULTI, false);// triple
			this.drawArc(angle, i, CONST.SANDARD_1, CONST.TRIPLE_RING, COLOR.TYPE_STANDARD, false);
			this.drawPoints(angle, i);
			angle -= CONST.PART_ANGLE;
		}
		this.drawCircle(CONST.BULL, CONST.SANDARD_1, COLOR.MULTI[1]);
		this.drawCircle(CONST.BULLS_EYE, CONST.BULL, COLOR.MULTI[0]);
	}
	
	getStartAngle() {
		return 0 - (Math.PI / 2) - (CONST.PART_ANGLE / 2);
	}
	
	drawMarked(endingsArray) {
		
		if (endingsArray == undefined) {
			return;
		}
		
		let standardSet = new Set();
		let doubleSet = new Set();
		let tripleSet = new Set();
		let bull = false;
		let bullsEye = false;
		let i;
		let tmp;
		
		for (i in endingsArray) {
				
			tmp = endingsArray[i][0];
			
			if (tmp.multi == 1) {
				if (tmp.points == 50) {
					bullsEye = true;
				}
				else if (tmp.points == 25) {
					bull = true;
				}
				else {
					standardSet.add(tmp.points);
				}
			}
			else if (tmp.multi == 2) {
				doubleSet.add(tmp.points);
			}
			else if (tmp.multi == 3) {
				tripleSet.add(tmp.points);
			}
		}
		
		let angle = this.getStartAngle();
		for (i in POINTS) {
			if (standardSet.has(POINTS[i])) {
				this.drawArc(angle, i, CONST.SANDARD_2, CONST.DOUBLE_RING, COLOR.TYPE_STANDARD, true);
				this.drawArc(angle, i, CONST.SANDARD_1, CONST.TRIPLE_RING, COLOR.TYPE_STANDARD, true);
			}
			angle += CONST.PART_ANGLE;
		}
		
		angle = this.getStartAngle();
		for (i in POINTS) {
			if (doubleSet.has(POINTS[i])) {
				this.drawArc(angle, i, CONST.DOUBLE_RING, CONST.BOARD_END, COLOR.TYPE_MULTI, true);// triple
			}
			angle += CONST.PART_ANGLE;
		}
		
		angle = this.getStartAngle();
		for (i in POINTS) {
			if (tripleSet.has(POINTS[i])) {
				this.drawArc(angle, i, CONST.TRIPLE_RING, CONST.SANDARD_2, COLOR.TYPE_MULTI, true);// double
			}
			angle += CONST.PART_ANGLE;
		}
		
		if (bull) {
			this.drawCircle(CONST.BULL, CONST.SANDARD_1, COLOR.MARKED_MULTI[1]);
		}
		if (bullsEye) {
			this.drawCircle(CONST.BULLS_EYE, CONST.BULL, COLOR.MARKED_MULTI[0]);
		}
	}
	
	drawArc(startAngle, i, startR, endR, type, marked) {
		// context.arc(x,y,r,sAngle,eAngle,counterclockwise);
		
		this.context.beginPath();
		
		if (marked)
			if (type == COLOR.TYPE_MULTI) {
				this.context.fillStyle = COLOR.MARKED_MULTI[i % 2];
			}
			else if (type == COLOR.TYPE_STANDARD) {
				this.context.fillStyle = COLOR.MARKED_STANDARD[i % 2];
			}
			else {
				console.log("Color type error!");
			}
		else {
			if (type == COLOR.TYPE_MULTI) {
				this.context.fillStyle = COLOR.MULTI[i % 2];
			}
			else if (type == COLOR.TYPE_STANDARD) {
				this.context.fillStyle = COLOR.STANDARD[i % 2];
			}
			else {
				console.log("Color type error!");
			}
		}
		
		this.context.arc(this.R, this.R, this.r * startR, startAngle , startAngle + CONST.PART_ANGLE, false);
		this.context.arc(this.R, this.R, this.r * endR, startAngle + CONST.PART_ANGLE , startAngle, true);
		this.context.fill();
	}
	
	drawCircle(startR, endR, color) {
		this.context.beginPath();
		this.context.fillStyle = color;
		this.context.arc(this.R, this.R, this.r * startR, 0 , 2 * Math.PI, false);
		this.context.arc(this.R, this.R, this.r * endR, 0 , 2 * Math.PI, true);
		this.context.fill();
	}
	
	drawPoints(startAngle, i) {
		let angle = startAngle - Math.PI / 2 + CONST.PART_ANGLE / 1.7;
		let fontSize = this.r * 0.1;
		let margin = 1.07;
		this.context.font = fontSize + "px Arial";
		this.context.fillStyle = "black";
		this.context.fillText(
			POINTS[i],
			this.R - fontSize / 2 + this.r * Math.sin(angle) * margin,
			this.R + fontSize / 2 + this.r * Math.cos(angle) * margin
		);
	}
	
	drawShots(currentShots, endings) {
	
		this.draw();
		this.drawMarked(endings);
		
		let shots = currentShots.shots.slice();
		let currentShot;
		let index;
		let occurrenceCounter;
	
		while (shots.length > 0) {
			
			currentShot = shots.pop();
			
			if (currentShot == null) {
				continue;
			}
			
			occurrenceCounter = 1;
			
			for (let i in shots) {
				if (currentShot.equals(shots[i])) {
					occurrenceCounter++;
					shots[i] = null;
				}
			}
			
			index = POINTS_INDEXES[currentShot.points - 1];
			
			if (currentShot.multi == 1) {
				if (currentShot.points == 50) {
					this.drawShotsMiddle(occurrenceCounter, true);
				}
				else if (currentShot.points == 25) {
					this.drawShotsMiddle(occurrenceCounter, false);
				}
				else {
					this.drawShotsStandard(index, occurrenceCounter);
				}
			}
			else if (currentShot.multi == 2) {
				this.drawShotsInRing(index, occurrenceCounter, this.doubleRingR);
			}
			else {
				this.drawShotsInRing(index, occurrenceCounter, this.tripleRingR);
			}
		}
		
		if (currentShots.shots.length > 3) {
			this.countButton.disabled = true;
		}
		else {
			this.countButton.disabled = false;
		}
	}
	
	drawShotsInRing(part, numberOfShots, ringR) {
	
		if (numberOfShots == 0) {
			// no job
			return;
		}
		
		let angle = part * CONST.PART_ANGLE;
		
		if (numberOfShots == 1) {
			this.drawPoint(
				Math.sin(angle) * ringR + this.R,
				this.R - Math.cos(angle) * ringR
			);
		}
		else if (numberOfShots == 2) {
			let angleStep = CONST.PART_ANGLE / 4;
			this.drawPoint(
				Math.sin(angle - angleStep) * ringR + this.R,
				this.R - Math.cos(angle - angleStep) * ringR
			);
			this.drawPoint(
				Math.sin(angle + angleStep) * ringR + this.R,
				this.R - Math.cos(angle + angleStep) * ringR
			);
		}
		else if (numberOfShots == 3) {
			let angleStep = CONST.PART_ANGLE / 3;
			this.drawPoint(
				Math.sin(angle - angleStep) * ringR + this.R,
				this.R - Math.cos(angle - angleStep) * ringR
			);
			this.drawPoint(
				Math.sin(angle) * ringR + this.R,
				this.R - Math.cos(angle) * ringR
			);
			this.drawPoint(
				Math.sin(angle + angleStep) * ringR + this.R,
				this.R - Math.cos(angle + angleStep) * ringR
			);
		}
		else {
			console.log("Error! Number of shots: " + numberOfShots);
		}
	}
	
	drawShotsStandard(part, numberOfShots) {
	
		if (numberOfShots == 0) {
			// no job
			return;
		}
		
		let angle = part * CONST.PART_ANGLE;
		
		if (numberOfShots == 1) {
			this.drawPoint(
				Math.sin(angle) * this.standardR + this.R,
				this.R - Math.cos(angle) * this.standardR
			);
		}
		else if (numberOfShots == 2) {
			let standardStep = this.standardSize / 4;
			this.drawPoint(
				Math.sin(angle) * (this.standardR + standardStep) + this.R,
				this.R - Math.cos(angle) * (this.standardR + standardStep)
			);
			this.drawPoint(
				Math.sin(angle) * (this.standardR - standardStep) + this.R,
				this.R - Math.cos(angle) * (this.standardR - standardStep)
			);
		}
		else if (numberOfShots == 3) {
			let standardStep = this.standardSize / 3;
			this.drawPoint(
				Math.sin(angle) * (this.standardR + standardStep) + this.R,
				this.R - Math.cos(angle) * (this.standardR + standardStep)
			);
			this.drawPoint(
				Math.sin(angle) * this.standardR + this.R,
				this.R - Math.cos(angle) * this.standardR
			);
			this.drawPoint(
				Math.sin(angle) * (this.standardR - standardStep) + this.R,
				this.R - Math.cos(angle) * (this.standardR - standardStep)
			);
		}
		else {
			console.log("Error! Number of shots: " + numberOfShots);
		}
	}
	
	drawShotsMiddle(numberOfShots, bullsEye) {
	
		if (numberOfShots == 0) {
			// no job
			return;
		}
		
		if (numberOfShots == 1) {
			if (bullsEye) {
				this.drawPoint(this.R, this.R);
			}
			else {
				this.drawPoint(this.R, this.R - this.bullR);
			}
		}
		else if (numberOfShots == 2) {
			let bullsEyeR3 = this.bullsEyeR / 3;
			if (bullsEye) {
				this.drawPoint(this.R - bullsEyeR3, this.R);
				this.drawPoint(this.R + bullsEyeR3, this.R);
			}
			else {
				this.drawPoint(this.R - this.bullR, this.R);
				this.drawPoint(this.R + this.bullR, this.R);
			}
		}
		else if (numberOfShots == 3) {
			let bullsEyeR3 = this.bullsEyeR / 3;
			let bullR4 = this.bullR / 4;
			if (bullsEye) {
				this.drawPoint(this.R, this.R - bullsEyeR3);
				this.drawPoint(this.R - bullsEyeR3 * 0.866, this.R + bullsEyeR3 / 2);
				this.drawPoint(this.R + bullsEyeR3 * 0.866, this.R + bullsEyeR3 / 2);
			}
			else {
				this.drawPoint(this.R, this.R - this.bullR);
				this.drawPoint(this.R - this.bullR * 0.866, this.R + this.bullR / 2);
				this.drawPoint(this.R + this.bullR * 0.866, this.R + this.bullR / 2);
			}
		}
		else {
			console.log("Error! Number of shots: " + numberOfShots);
		}
	}
	
	drawPoint(x, y) {
		this.context.beginPath();
		this.context.fillStyle = COLOR.SHOT;
		this.context.arc(x, y, this.shotR, 0, 2 * Math.PI);
		this.context.fill(); 
	}

	onClick(clickX, clickY, callback) {

		let x = clickX - this.R;
		let y = clickY - this.R;
		//console.log("x=" + x + ", y=" + y);
		
		// edge cases
		// if x=0, trigonometry goes crazy :), x=1 is OK
		if (x == 0) {
			x = 1;
		}
		
		let absX = Math.abs(x);
		let absY = Math.abs(y);
		let realR = Math.sqrt(absX * absX + absY * absY);
		
		if (realR > this.r) {
			//console.log("Out!");
		}
		else if (realR > this.r * CONST.DOUBLE_RING) {
			callback.onNewPoints(this.getClickedPart(x, y), 2);
		}
		else if (realR > this.r * CONST.SANDARD_2) {
			callback.onNewPoints(this.getClickedPart(x, y));
		}
		else if (realR > this.r * CONST.TRIPLE_RING) {
			callback.onNewPoints(this.getClickedPart(x, y), 3);
		}
		else if (realR > this.r * CONST.SANDARD_1) {
			callback.onNewPoints(this.getClickedPart(x, y));
		}
		else if (realR > this.r * CONST.BULL) {
			callback.onNewPoints(25);
		}
		else {
			callback.onNewPoints(50);
		}
	}
	
	getClickedPart(x, y) {
		let angle = Math.atan(y / x);
		let i = angle / (CONST.PART_ANGLE / 2) + 10;
		let j = Math.floor((i - 1) / 2) + 1;
		return POINTS[x > 0 ? j : (j + 10) % 20];
	}
	
}