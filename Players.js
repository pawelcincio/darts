class Players {
	
	constructor(table, lang) {
		this.table = table;
		this.lang = lang;
		
		this.delimiter = "|";
		
		this.names = [];
		this.pushName("Gracz 1");
		this.pushName("Gracz 2");
		this.updateView();
	}
	
	get names() {
		return this._names;
	}
	
	set names(value) {
		this._names = value;
	}
	
	pushName(value) {
		this.names.push(value);
	}
	
	add(name) {
		this.updateNames();
		if (name) {
			this.pushName(name);
		}
		else {
			this.pushName("");
		}
		this.updateView();
	}
	
	remove(index) {
	
		this.updateNames();
		
		if (index > -1) {
			this.names.splice(index, 1);
		}
		else {
			console.log("Błąd usuwania gracza, index = ${index}");
		}
		
		this.updateView();
	}
	
	updateView() {
		
		let innerHTML = "";
		let i;
		
		for (i in this.names) {
			innerHTML += this.makeRow(i);
		}
		
		this.table.innerHTML = innerHTML;
	}
	
	makeRow(i) {
		return `<div id=\"div-table-row\">
			<div id=\"div-table-left-col\">
			<input class=\"start-form-player-name\" type=\"text\" value=\"${this.names[i]}\">
			</div>
			<div id=\"div-table-right-col\">
			<button onclick=\"controller.removePlayer(${i})\">${i18n[this.lang].DELETE}</button>
			</div>
			</div>`;
	}
	
	updateNames() {
	
		let elements = document.getElementsByClassName('start-form-player-name');
		
		let i;
		
		for (i = 0; i < elements.length; i++) {
			this.names[i] = elements[i].value;
		}
	}
	
	validate() {
		
		this.updateNames();
		
		let isOK = true;
		let i;
		
		for (i in this.names) {
			
			this.names[i] = this.names[i].trim();
			
			if (!this.names[i]) {
				isOK = false;
			} 
		}
		
		if (isOK) {
			let uniqueNames = Array.from(new Set(this.names));
			isOK = uniqueNames.length == this.names.length;
		}
		
		return isOK;
	}
	
	toUrlList() {
		let result = "";
		for (let i in this.names) {
			result += this.names[i];
			if (i != this.names.length - 1) {
				result += this.delimiter;
			}
		}
		return result;
	}
	
}