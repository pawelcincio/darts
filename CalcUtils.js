class CalcUtils {
	
	static calculateEndings(pointsLeft, doubleEnding, numberOfTurns) {
		
		let result = [];
		
		if (typeof numberOfTurns == 'number' && numberOfTurns >= 1 && numberOfTurns <=3) {
			CalcUtils.calculateEndingsOnLevel(pointsLeft, doubleEnding, numberOfTurns, result, []);
		}
		
		return result;
	}
	
	static calculateEndingsOnLevel(pointsLeft, doubleEnding, level, result, shotsArray) {
		
		let tmp;
		let ending;
		let tmpShotsArray;
		
		for (let i in ALL_POSSIBLE_POINTS) {
			
			ending = ALL_POSSIBLE_POINTS[i];
			
			if (pointsLeft >= ending.getResult()) {
				
				tmpShotsArray = shotsArray.slice();
				tmpShotsArray.push(ending.getShot());
				
				tmp = pointsLeft - ending.getResult();
				
				if (tmp == 0) {
					if (doubleEnding) {
						if (ending.getShot().multi == 2 || (ending.getShot().points == 50 && level == 1)) {
							result.push(tmpShotsArray);
						}
					}
					else {
						result.push(tmpShotsArray);
					}
				}
				else if (level > 1) {
					CalcUtils.calculateEndingsOnLevel(tmp, doubleEnding, level - 1, result, tmpShotsArray.slice());
				}
			}
		}
	}

}