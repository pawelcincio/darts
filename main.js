// main app controller, there could be only one instance
var controller;

// point values listed clockwise from 20
const POINTS = [20, 1, 18, 4, 13, 6, 10, 15, 2, 17, 3, 19, 7, 16, 8, 11, 14, 9, 12, 5];
// all possible points player can reach in one shot
const ALL_POSSIBLE_POINTS = [];
// reversed version of array POINTS for faster operations
const POINTS_INDEXES = [];

// constant properties of board
const CONST = {
	BOARD_MARGIN_RATIO: 0.06,					// margin: board - window edge
	PART_ANGLE: (2 * Math.PI) / POINTS.length,	// ange of each part
	// board measurements, from 0 to 1:
	BULLS_EYE: 0,								// start from the middlw
	BULL: 0.1,									// end of bull's eye field (50)
	SANDARD_1: 0.2,								// end of bull field (25) and start of inner standard field
	TRIPLE_RING: 0.5,							// end of inner standard field and start of triple ring
	SANDARD_2: 0.6,								// end of triple ring and start of outer standard field
	DOUBLE_RING: 0.9,							// end of outer standard field and start of double ring
	BOARD_END: 1,								// end of double ring and end of board
	SHOT_R_RATIO: 0.02							// size of shot dot
}

// colors of board
const COLOR = {
	TYPE_MULTI: "multi",					// type of field - used as enum
	TYPE_STANDARD: "standard",				// type of field - used as enum
	MULTI: ["#ee0000", "#00ce00"],			// two colors of multi fields
	STANDARD: ["#101010", "#fff8ad"],		// two colors of standard fields
	MARKED_MULTI: ["#ffbf00", "#d9d900"],	// two colors of marked multi fields
	MARKED_STANDARD: ["#ffd24c", "#ffff00"],// two colors of marked standard fields
	SHOT: "#ff4cff"							// color of shot dot
}

// url param names, description in help directory
const PARAMS = {
	TYPE_OF_GAME: "typeOfGame",	// see TYPES_OF_GAME below
	PLAYERS: "players",			// list of player names
	START_NOW: "startNow",		// start game immediately omitting game settings (true/false)
	LANG: "lang"				// language of app
}

// amount of points to get
const TYPES_OF_GAME = [301, 501, 701];

// list of languages, see i18n.js file
const LANGS = Object.getOwnPropertyNames(i18n);

// calculate variable which are dependent from others
function initVariables() {
	
	let i;
	let j;
	
	for (i in POINTS) {
		POINTS_INDEXES[POINTS[i] - 1] = i;
	}
	
	for (i = 0; i < 3; i++) {
		for (j in POINTS) {
			ALL_POSSIBLE_POINTS.push(new PossiblePoint(POINTS[j], i + 1));
		}			
	}
	ALL_POSSIBLE_POINTS.push(new PossiblePoint(25));
	ALL_POSSIBLE_POINTS.push(new PossiblePoint(50));
}

function myOnLoad() {
	
	// read url params
	let urlParams = new URLSearchParams(window.location.search);
	let typeOfGame = urlParams.get(PARAMS.TYPE_OF_GAME);
	let listOfPlayers = urlParams.get(PARAMS.PLAYERS);
	let startNow = urlParams.get(PARAMS.START_NOW);
	let lang = urlParams.get(PARAMS.LANG);
	
	// default language should be "EN"
	let languageId = 0;
	
	// check lang value
	if (lang) {
		let tmpLangId = null;
		for (let i in LANGS) {
			if (LANGS[i] == lang) {
				tmpLangId = i;
			}
		}
		if (tmpLangId == null) {
			alert(`Parameter "${PARAMS.LANG}" has to have one of theese values: ${LANGS}.`);
		}
		else {
			languageId = tmpLangId;
		}
	}
	lang = LANGS[languageId];
	
	// load list of languages and support change of language
	let langSelect = document.getElementById("lang-select");
	for (let i in LANGS) {
		langSelect.options[i] = new Option(LANGS[i], LANGS[i]);
	}
	langSelect.value = lang;
	langSelect.onchange = function() {
		let urlParams = new URLSearchParams(window.location.search);
		urlParams.set("lang", LANGS[this.selectedIndex]);
		window.location.search = urlParams.toString();
	}
	
	// load list of TYPES_OF_GAME
	let typeOfGameSelect = document.getElementById("start-form-points");
	for (let i in TYPES_OF_GAME) {
		typeOfGameSelect.options[i] = new Option(TYPES_OF_GAME[i], TYPES_OF_GAME[i]);
	}
	
	// load all constant variables
	initVariables();
	// create main controller
	controller = new Controller(typeOfGame, listOfPlayers, startNow, lang);
}

// redraw view using different window size
window.onresize = function(event) {
	if (controller != null) {
		controller.onPageResize();
	}
};