class ModalWindowController {
	
	constructor(modalWindow, canvas, countButton, sizeSource, shotsArray, i, j) {
		
		this.modalWindow = modalWindow;
		this.currentShots = new CurrentShots();
		this.currentShots.shots = shotsArray.slice();
		this.i = i;
		this.j = j;
		
		this.board = new Board(canvas, countButton, sizeSource);
		this.board.draw();
		this.board.drawShots(this.currentShots);
	}
	
	onPageResize() {
		this.board.recalculate();
		this.board.drawShots(this.currentShots);
	}
	
	boardClick(clickX, clickY) {
		this.board.onClick(clickX, clickY, this);
	}

	// called when we have to add new points, called from board the object,
	// after calculating points from coursor co-ordinates
	onNewPoints(points, multi) {
		this.currentShots.add(new Shot(points, multi));
		this.board.drawShots(this.currentShots);
	}
	
	getCurrentShots() {
		return this.currentShots;
	}
	
	getI() {
		return this.i;
	}
	
	getJ() {
		return this.j;
	}
	
}