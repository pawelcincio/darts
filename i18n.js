const i18n = {
	
	EN: {
		GAME_PARAMETERS: "Game Parameters",
		NUMBER_OF_POINTS: "Number of Points",
		LIST_OF_PLAYERS: "List of Players",
		DELETE: "Delete",
		ADD_PLAYER: "Add Player",
		START_GAME: "Start Game",
		COUNT: "Count",
		EDIT_TOUR: "Edit Tour",
		SAVE: "Save"
	},
	
	PL: {
		GAME_PARAMETERS: "Parametry gry",
		NUMBER_OF_POINTS: "Liczba punktów",
		LIST_OF_PLAYERS: "Lista graczy",
		DELETE: "Usuń",
		ADD_PLAYER: "Dodaj gracza",
		START_GAME: "Rozpocznij grę",
		COUNT: "Podlicz",
		EDIT_TOUR: "Edytuj turę",
		SAVE: "Zapisz"
	}
	
};