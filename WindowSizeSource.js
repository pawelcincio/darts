class WindowSizeSource {
	
	constructor() {
	}
	
	getWidth() {
		return window.innerWidth;
	}
	
	getHeight() {
		return window.innerHeight;
	}
	
}