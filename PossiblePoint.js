class PossiblePoint {
	
	constructor(points, multi) {
		this.shot = new Shot(points, multi);
		this.result = this.shot.getResult();
	}
	
	getResult() {
		return this.result;
	}
	
	getShot() {
		return this.shot;
	}
	
}