class Shot {
	
	constructor(points, multi = 1) {
		this.points = points;
		this.multi = multi;
	}
	
	set points(points) {
		
		if (!points || (points != 50 && points != 25 && (points < 1 || points > 20))) {
			console.log("Amount of points \"${points}\" is incorrect!");
		}
		else {
			this._points = points;
		}
	}
	
	get points() {
		return this._points;
	}
	
	set multi(multi) {
		
		if (!multi || (multi < 1 && multi > 3)) {
			console.log("Multiplier cannot be \"${multi}\"");
		}
		else {
			this._multi = multi;
		}
	}
	
	get multi() {
		return this._multi;
	}
	
	countPoints() {
		return this.points * this.multi;
	}
	
	static countPoints(shots) {
		
		if (shots.length == 0) {
			return 0;
		}
		
		let points = 0;
		let i;
		
		for (i in shots) {
			points += shots[i].countPoints();
		}
		
		return points;
	}
	
	getResult() {
		return this._points * this._multi;
	}
	
	equals(anotherShot) {
		return anotherShot != null && this.points == anotherShot.points && this.multi == anotherShot.multi;
	}
	
	toString() {
		if (this.multi > 1) {
			return this.points + "x" + this.multi;
		}
		else {
			return this.points + "x1";
		}
	}
	
}